### Install dependencies
This project has the following dependencies:
* nodejs
* mongodb
* a few nodejs libraries

You need to run the following comands in order to install dependencies needed
by this project.

#### Install nodejs
```
sudo apt-get install nodejs npm
```

#### Install mongodb
```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install mongodb
```

#### Install nodejs libraries
From the root directory of the project, run:
```
npm install
```

### Start
Start the server:
```
nodejs server.js
```

Now open your browser and go to http://localhost:3000

