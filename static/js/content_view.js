var BackButton = React.createClass({
    handleClick: function(event) {
	Actions.goBackFromContent();
    },

    render: function() {
	return (
	    <a className="button" onClick={this.handleClick}>
	    Volver
	    </a>
	);
    }
});

var ContentView = React.createClass({
    render: function() {
	var content = Store.getCurrentContent();
	return (
	    <div>
		<p>Titulo: {content.title}</p>
		<p>Beacon ID: {content.beacon_id}</p>
		<p>Informacion: {content.information}</p>
               <BackButton />	
            </div>
	);
    }

});