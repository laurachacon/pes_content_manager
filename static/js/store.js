var Store = Marty.createStore ({
    handlers: {
	cancelNewContent: Constants.CANCEL_NEW_CONTENT,
	createNewContent: Constants.CREATE_NEW_CONTENT,
	deleteContent: Constants.DELETE_CONTENT,
	editContent: Constants.EDIT_CONTENT,
	goBackFromContent: Constants.GO_BACK_FROM_CONTENT,
	goToContent: Constants.GO_TO_CONTENT,
	goToEditContent: Constants.GO_TO_EDIT_CONTENT,
	goToNewContent: Constants.GO_TO_NEW_CONTENT
    },

    getInitialState: function() {
	return {
	    contents: getContentsFromServer(),
	    currentScreen: "manageAllContent",
	    currentContentId: ""
	};
    },


    // ACTIONS -----------------------------------------------------------------

    cancelNewContent: function() {
	this.state.currentScreen = "manageAllContent";
	this.hasChanged();
    },

    createNewContent: function(title, beaconId, information) {
	postContentToServer(title, beaconId, information);
	this.state.currentScreen = "manageAllContent";
	this.state.contents = getContentsFromServer();
	this.hasChanged();
    },

    deleteContent: function(contentId) {
	deleteContentFromServer(contentId);
	this.state.contents = getContentsFromServer();
	this.hasChanged();
    },
    
    editContent: function(title, beaconId, information) {
	editContentFromServer(this.state.currentContentId, 
			      title, 
			      beaconId, 
			      information);
	this.state.currentScreen = "manageAllContent";
	this.state.contents = getContentsFromServer();
	this.hasChanged();
    },

    goBackFromContent: function() {
	this.state.currentScreen = "manageAllContent";
	this.hasChanged();
    },

    goToContent: function(contentId) {
	this.state.currentScreen = "content";
	this.state.currentContentId = contentId;
	this.hasChanged();
    },

    goToEditContent: function(contentId) {
    	this.state.currentScreen = "editContent";
	this.state.currentContentId = contentId;
	this.hasChanged();
    },  

    goToNewContent: function() {
	this.state.currentScreen = "newContent";
	this.hasChanged();
    },


    // GETTERS -----------------------------------------------------------------

    getContents: function() {
	return this.state.contents;
    },

    getCurrentContent: function() {
	for(var i = 0; i < this.state.contents.length; i++) {
	    var content = this.state.contents[i];
	    if(this.state.currentContentId === content._id) {
		return content;
	    }
	}
    },

    getCurrentScreen: function() {
	return this.state.currentScreen;
    }
});


// AUX FUNCTIONS ---------------------------------------------------------------

function getContentsFromServer() {
    var contents;
    $.ajax({
	type: 'GET',
	url: '/contents',
	success: function(response) {
	    contents = response;
	},
	async: false
    });
    return contents;
}

function postContentToServer(title, beaconId, information) {
    $.ajax({
	type: 'POST',
	url: "/contents",
	data: JSON.stringify({
	    title: title,
	    beacon_id: beaconId,
	    information: information
	}),
	contentType: 'application/json',
	async: false
    });
}

function deleteContentFromServer(contentId) {
    $.ajax({
	type: 'DELETE',
	url: '/contents/' + contentId,
	async: false
    });   
}

function editContentFromServer(contentId, title, beaconId, information) {
    $.ajax({
	type: 'PUT',
	url: '/contents/' + contentId,
	data: JSON.stringify({
	    title: title,
	    beacon_id: beaconId,
	    information: information
	}),
	contentType: 'application/json',
	async: false,
	success: function(data) {
	    alert('La edicion se ha hecho correctamente');
	}
    });
}