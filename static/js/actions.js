var Actions = Marty.createActionCreators({
    cancelNewContent: Constants.CANCEL_NEW_CONTENT(
	function() {
	    this.dispatch();
	}),
    
    createNewContent: Constants.CREATE_NEW_CONTENT(
	function(title, beaconId, information) {
	    this.dispatch(title, beaconId, information);
	}),
    
    deleteContent: Constants.DELETE_CONTENT(
	function(contentId) {
	    this.dispatch(contentId);
	}),

    editContent: Constants.EDIT_CONTENT(
	function(title, beaconId, information) {
	    this.dispatch(title, beaconId, information);
	}),

    goBackFromContent: Constants.GO_BACK_FROM_CONTENT(
	function() {
	    this.dispatch();
	}),

    goToContent: Constants.GO_TO_CONTENT(
	function(contentId) {
	    this.dispatch(contentId);
	}),

    goToEditContent: Constants.GO_TO_EDIT_CONTENT(
    	function(contentId) {
    	    this.dispatch(contentId);
	}),	
    
    goToNewContent: Constants.GO_TO_NEW_CONTENT(
	function() {
	    this.dispatch();
	})
});