var Constants = Marty.createConstants([
    'CANCEL_NEW_CONTENT',
    'CREATE_NEW_CONTENT',
    'DELETE_CONTENT',
    'EDIT_CONTENT',
    'GO_BACK_FROM_CONTENT',
    'GO_TO_CONTENT',
    'GO_TO_EDIT_CONTENT',
    'GO_TO_NEW_CONTENT'
]);