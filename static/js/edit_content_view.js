var EditButton = React.createClass({
    render: function() {
	return (
	    <a className="button" onClick={this.props.handleClick}>
	    Editar
	    </a>
	);
    }    
});

var BackButton = React.createClass({
    handleClick: function(event) {
	Actions.goBackFromContent();
    },

    render: function() {
	return (
	    <a className="button" onClick={this.handleClick}>
	    Volver
	    </a>
	);
    }
});

var EditContentView = React.createClass({
    handleEditButtonClick: function(event) {
	var title = this.refs.title.getDOMNode().value;
	var beacon_id = this.refs.beacon_id.getDOMNode().value;
	var information = this.refs.information.getDOMNode().value;
	Actions.editContent(title, beacon_id, information);
    },

    render: function() {
	var content = Store.getCurrentContent();
	return (
	    <div>
		<p>Titulo: </p>
		<textarea name="title" 
			  rows="1" 
			  cols="18" 
			  ref="title">
				{content.title}
		</textarea>
		<p>Beacon ID: </p>
		<textarea name="beacon_id" 
			  rows="1" 
			  cols="18" 
			  ref="beacon_id">
				{content.beacon_id}
		</textarea>
		<p>Informacion: </p> 
		<textarea name="information" 
			  rows="4" 
			  cols="50" 
			  ref="information">
				{content.information}
		</textarea>
		<br/>
		<EditButton handleClick={this.handleEditButtonClick}/>
               <BackButton />	
            </div>
	);
    }

});