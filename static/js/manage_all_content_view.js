var DeleteContentButton = React.createClass({
    handleClick: function(event) {
	Actions.deleteContent(this.props.contentId);
    },
    
    render: function() {
	return (
            <a className="button" onClick={this.handleClick}>
	    Borrar
	    </a>
	);
    }
});

var ViewContentButton = React.createClass({
    handleClick: function(event) {
	Actions.goToContent(this.props.contentId);
    },
    
    render: function() {
	return (
            <a className="button" onClick={this.handleClick}>
	    Consultar
	    </a>
	);
    }
});


var EditContentButton = React.createClass({
    handleClick: function(event) {
	Actions.goToEditContent(this.props.contentId);
    },
    
    render: function() {
	return (
            <a className="button" onClick={this.handleClick}>
	    Editar
	    </a>
	);
    }
});

var ContentListItem = React.createClass({
    render: function() {
	var content = this.props.content;
	return (
            <div>
		{content.title}
                <DeleteContentButton contentId={content._id}/>
	        <ViewContentButton contentId={content._id}/>
	        <EditContentButton contentId={content._id}/>
                <br/>
            </div>
	);
    }
});



var ContentList = React.createClass({
    render: function() {
	var contents = Store.getContents();
	var rows = [];
	for (var i = 0; i < contents.length; i++) {
	    rows.push(<ContentListItem content={contents[i]}/>);
	}
	return (
	    <div>
               <p id="subtitle">Lista de contenidos</p>
		{rows}
            </div>
	);
    }
});

var AddContentButton = React.createClass({
    handleClick: function(event) {
	Actions.goToNewContent();
    },

    render: function() {
	return (
	    <a className="button" onClick={this.handleClick}>
	    A&ntilde;adir una obra nueva
	    </a>
	);
    }
});

var ManageAllContentView = React.createClass({
    render: function() {
	return (
	    <div>
               <AddContentButton />
               <ContentList />
            </div>
	);
    }

});