var CreateButton = React.createClass({
    render: function() {
	return (
	    <a className="button" onClick={this.props.handleClick}>
	    Crear
	    </a>
	);
    }
});

var CancelButton = React.createClass({
    handleClick: function(event) {
	Actions.cancelNewContent();
    },

    render: function() {
	return (
	    <a className="button" id="cancel" onClick={this.handleClick}>
	    Cancelar
	    </a>
	);
    }
});

var NewContentForm = React.createClass({
    handleCreateButtonClick: function(event) {
	var title = this.refs.title.getDOMNode().value;
	var beacon_id = this.refs.beacon_id.getDOMNode().value;
	var information = this.refs.information.getDOMNode().value;
	Actions.createNewContent(title, beacon_id, information);	
    },

    render: function() {
	return (
	    <div>
	    <form>
		T&iacute;tulo:
		<br/>
		<input type="text"
                       name="title"
                       ref="title"/>
		<br/>
		Beacon ID:
		<br/>
		<input type="text"
                       name="beacon_id"
                       ref="beacon_id"/>
		<br/>
		Informaci&oacute;n:
		<br/>
		<textarea name="information"
                          rows="4"
                          cols="50"
                          ref="information">
		</textarea>
		<br/>
	    </form>
            <CreateButton handleClick={this.handleCreateButtonClick}/>
            <CancelButton />
            </div>
	);
    }
});

var NewContentView = React.createClass({
    render: function() {
	return (
	    <div id="newContentView">
		<p id="subtitle">Nuevo contenido:</p>
		<NewContentForm />
	    </div>
	);
    }

});