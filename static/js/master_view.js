var MasterView = React.createClass({
    getInitialState: function() {
	return {
	    currentScreen: Store.getCurrentScreen()
	};
    },

    componentDidMount: function() {
	this.storeListener = Store.addChangeListener(this.onStoreChanged);
    },

    componentWillUnmount: function() {
	this.storeListener.dispose();
    },

    onStoreChanged: function() {
	this.setState({
	    currentScreen: Store.getCurrentScreen()
	});
    },

    render: function() {
       var screen;
       if(this.state.currentScreen === "manageAllContent") {
	   screen = <ManageAllContentView />;
       }
       else if(this.state.currentScreen === "newContent") {
	   screen = <NewContentView />;
       }
       else if(this.state.currentScreen === "content") {
	   screen = <ContentView />;
       }
       else if(this.state.currentScreen ==="editContent") {
       	    screen = <EditContentView />
       }      
       return (
           <div className="masterView">
	       <p id="title">Gesti&oacute;n de contenido</p>
	       {screen}
	   </div>
       );
   }
});
