var express = require('express');
var bodyParser = require('body-parser');
var serveStatic = require('serve-static');
var morgan = require('morgan');
var mongoClient = require('mongodb').MongoClient;
var mongodb = require('mongodb');
var assert = require('assert');
var contentsCollection;


// SETUP -----------------------------------------------------------------------

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(serveStatic('static', {'index': ['index.html']}));
var server = app.listen(3000, function() {});

mongoClient.connect("mongodb://localhost:27017/mydb", function(err, db) {
    if(!err) {
	console.log("Successfully connected to mongodb");
	contentsCollection = db.collection('contents');
    }
    else {
	console.log("Error " + err);
    }
});


// REQUEST HANDLERS ------------------------------------------------------------

app.get('/contents', function(req, res) {
    contentsCollection.find().toArray(function(err, items) {
	res.send(items);
    });
});

app.post('/contents', function(req, res) {
    writeContent(req.body);
    res.sendStatus(201);
});

app.put('/contents/:content_id', function(req, res) {
    editContent(req.body, req.params.content_id);
    res.sendStatus(201);
});

app.delete('/contents/:content_id', function(req, res) {
    deleteContent(req.params.content_id);
    res.sendStatus(200);
});

// AUX FUNCTIONS ---------------------------------------------------------------

function writeContent(content) {
    contentsCollection.insert(content, {w: 1}, function(err, result) {
	console.log("-------------------------------------------------");
	console.log("DB write done");
	console.log("Error: " + err);
	console.log("Result: " + JSON.stringify(result));
	console.log("Object: " + JSON.stringify(content));
	console.log("-------------------------------------------------");
    });
}

function deleteContent(contentId) {
    contentsCollection.remove(
	{'_id': mongodb.ObjectID(contentId)},
	{w: 1},
	function(err, numberOfRemovedObjects) {
	    assert.equal(1, numberOfRemovedObjects);
	    console.log("-------------------------------------------------");
	    console.log("DB delete done");
	    console.log("Error: " + err);
	    console.log("Number of removed objects: " + numberOfRemovedObjects);
	    console.log("ID: " + contentId);
	    console.log("-------------------------------------------------");
    });
}

function editContent(content, contentId) {
    contentsCollection.update(
	{'_id': mongodb.ObjectID(contentId)},
	{$set: 
	    {
	     title: content.title, 
	     beacon_id: content.beacon_id,
	     information: content.information
	 }
	},
	{w: 1},
	function(err, result) {
	    console.log("-------------------------------------------------");
	    console.log("DB edit done");
	    console.log("Error: " + err);
	    console.log("Result: " + JSON.stringify(result));
	    console.log("Object: " + JSON.stringify(content));
	    console.log("-------------------------------------------------");
	}
    );
}